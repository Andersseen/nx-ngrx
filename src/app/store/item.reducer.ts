import { createReducer, createSelector, on } from '@ngrx/store';
import { addItem, getItem } from './item.actions';

export interface ItemState {
  items: Item[];
}

export interface Item {
  itemId: number;
  name: string;
}

const initialState: Item[] = [];

export const itemReducer = createReducer(
  initialState,
  on(getItem, (state, { items }) => [...items]),
  on(addItem, (state, { item }) => [...state, item])
);

export const itemSelector = createSelector(
  (state: ItemState) => state.items,
  (items) => items
);
