import { createAction } from '@ngrx/store';
import { Item } from './item.reducer';

export const getItem = createAction('[Item] Get Item', (items: Item[]) => ({
  items,
}));
export const addItem = createAction('[Item] Add Item', (item: Item) => ({
  item,
}));
