import { Component } from '@angular/core';
import { ItemComponent } from './item/item.component';
import { addItem } from './store/item.actions';
import { Store } from '@ngrx/store';
import { Item, ItemState, itemSelector } from './store/item.reducer';
import { CommonModule } from '@angular/common';

@Component({
  standalone: true,
  imports: [CommonModule, ItemComponent],
  selector: 'nx-ngrx-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'nx-ngrx';
  items$ = this.store.select(itemSelector);

  constructor(private store: Store<ItemState>) {}

  createUser() {
    const item: Item = {
      itemId: 1,
      name: 'Algo',
    };
    this.store.dispatch(addItem(item));
  }
}
