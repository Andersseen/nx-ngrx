import { ApplicationConfig } from '@angular/core';
import {
  provideRouter,
  withEnabledBlockingInitialNavigation,
} from '@angular/router';
import { appRoutes } from './app.routes';
import { provideStore } from '@ngrx/store';
import { provideEffects } from '@ngrx/effects';
import { itemReducer } from './store/item.reducer';

export const appConfig: ApplicationConfig = {
  providers: [
    provideEffects(),
    provideStore({ items: itemReducer }),
    provideRouter(appRoutes, withEnabledBlockingInitialNavigation()),
  ],
};
