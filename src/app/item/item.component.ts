import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'nx-ngrx-item',
  standalone: true,
  imports: [RouterModule],
  templateUrl: './item.component.html',
})
export class ItemComponent {}
